<?php

/**
 * @file
 * Per-bundle storage implementation using the Field Storage API "pre"
 * hooks and other Field API hooks.
 */

/**
 * @defgroup field_pbs Field API Per-Bundle Storage
 * @{
 * Store multiple Field API data fields in a wide table so that they
 * can be loaded for a single object in a single query instead of
 * requiring multiple queries or joins.
 */
/**
 * @} End of "defgroup field_pbs"
 */

/**
 * If FALSE, all data stored in the per-bundle tables will also be
 * stored by the default field storage engine.  If TRUE, all
 * non-shared fields will only be stored in per-bundle tables, and
 * shared fields will be stored based on PBS_DUPLICATE_SHARED_FIELDS.
 *
 * NOTE: This module no longer supports TRUE for this setting due to
 * (at least) lack of hook_field_attach_query() and bulk delete support.
 */
define('PBS_PRIMARY_STORAGE', FALSE);

/**
 * If TRUE, shared field data will be stored both in the per-bundle
 * table for all of the field's bundles plus the default field storage
 * engine.  If FALSE, shared fields are only stored in per-bundle
 * tables, just as non-shared fields are.
 */
define('PBS_DUPLICATE_SHARED_FIELDS', FALSE);

/**
 * Generate a table name for a bundle data table.
 *
 * @param $name
 *   The name of the bundle
 * @return
 *   A string containing the generated name for the database table
 */
function pbs_tablename($name) {
  return 'field_pbs_' . $name;
}

/**
 * Generate a table name for a bundle revision archive table.
 *
 * @param $name
 *   The name of the bundle
 * @return
 *   A string containing the generated name for the database table
 */
function pbs_revision_tablename($name) {
  return 'field_pbsr_' . $name;
}

/**
 * Return the database schema for a bundle table and its corresponding
 * revision table.  A bundle table has columns to store every
 * non-unlimited field's data in the bundle.
 *
 * @param $bundle
 *   The bundle name for which to create table schemas.
 * @return
 *   One or more tables representing the schema for the bundle tables.
 */
function pbs_bundle_schema($bundle) {
  $current = array(
    'description' => 'Bundle table for bundle '. $bundle,
    'fields' => array(
      'etid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity type id this data is attached to',
      ),
      'entity_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'The entity id this data is attached to',
      ),
      'revision_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
      ),
      'language' => array(
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The language for this data',
      ),
    ),
    'primary key' => array('etid', 'entity_id', 'language'),
  );

  $instances = field_info_instances($bundle);
  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      for ($i = 0; $i < $field['cardinality']; ++$i) {
        foreach ($schema['columns'] as $name => $spec) {
          $column_name = $field['field_name'] .'_'. $name .'_'. $i;
          $current['fields'][$column_name] = $spec;
        }
      }
    }
  }

  // Construct the revision table.  The primary key includes
  // revision_id but not entity_id so that multiple revision loads can
  // use the IN operator.
  $revision = $current;
  $revision['description'] = 'Revision archive for bundle table for bundle '. $bundle;
  $revision['revision_id']['description'] = 'The entity revision id this data is attached to';
  $revision['primary key'] = array('etid', 'revision_id', 'language');

  return array(
    pbs_tablename($bundle) => $current,
    pbs_revision_tablename($bundle) => $revision,
  );
}

/**
 * Create the per-bundle storage table for a bundle if it does not
 * already exist.
 *
 * @param $bundle
 *   The name of the bundle.
 */
function pbs_create_bundle_tables($bundle) {
  $schema = pbs_bundle_schema($bundle);
  foreach ($schema as $name => $table) {
    if (!db_table_exists($name)) {
      db_create_table($ret, $name, $table);
    }
  }
}

/**
 * Pre-compute the mapping of bundle table column name to field, item,
 * and delta.  This must be called any time the set of field instances
 * changes in any way.
 *
 * Suppose that a field name F, id Fid, with columns C1 and C2 and
 * with cardinality 2 has instances for bundles B1 and B2.  For field
 * F, the map will contain:
 *
 * $map = array(
 *   'B1' => array(
 *     'F_C1_1' => array('F', 'Fid', 'C1', 1),
 *     'F_C1_2' => array('F', 'Fid', 'C1', 2),
 *     'F_C2_1' => array('F', 'Fid', 'C2', 1),
 *     'F_C2_2' => array('F', 'Fid', 'C2', 2),
 *     // other entries for B1 
 *   ),
 *   'B2' => array(
 *     'F_C1_1' => array('F', 'Fid', 'C1', 1),
 *     'F_C1_2' => array('F', 'Fid', 'C1', 2),
 *     'F_C2_1' => array('F', 'Fid', 'C2', 1),
 *     'F_C2_2' => array('F', 'Fid', 'C2', 2),
 *     // other entries for B2
 *   ),
 * );
 */
function pbs_precompute_bundle_map() {
  $map = array();

  // Gather fields to load for each bundle.
  foreach (field_info_bundles() as $bundle => $label) {
    $instances = field_info_instances($bundle);
    foreach ($instances as $instance) {
      $field = field_info_field($instance['field_name']);
      if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
        $schema = (array) module_invoke($field['module'], 'field_schema', $field);
        for ($delta = 0; $delta < $field['cardinality']; ++$delta) {
          foreach ($schema['columns'] as $name => $spec) {
            $column_name = _field_sql_storage_columnname($field['field_name'], $name) .'_'. $delta;
            $map[$bundle][$column_name] = array($field['field_name'], $field['id'], $name, $delta);
          }
        }
      }
    }
  }
    
  variable_set('pbs_field_map', $map);
  return $map;
}

/**
 * Initialize a single bundle table from its constituent field
 * tables.
 *
 * @param $bundle
 *   The bundle to initialize.
 */
function pbs_initialize_bundle($bundle) {
  $bundle_table = pbs_tablename($bundle);
  $revision_table = pbs_revision_tablename($bundle);

  $instances = field_info_instances($bundle);
  foreach ($instances as $instance) {
    $field = field_info_field($instance['field_name']);
    if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      $field_table = _field_sql_storage_tablename($field);
      $field_revision_table = _field_sql_storage_revision_tablename($field);
      _pbs_initialize_bundle_field($field, 'entity_id', $bundle_table, $field_table);
      _pbs_initialize_bundle_field($field, 'entity_id', $revision_table, $field_revision_table);
    }
  }
}

function _pbs_initialize_bundle_field($field, $id_key, $bundle_table, $field_table) {
  $pkey = array('etid' => 0, $id_key => 1, 'language' => 2);
  $results = db_select($field_table, 't', array('fetch' => PDO::FETCH_ASSOC))
    ->fields('t')
    ->condition('bundle', $bundle)
    ->condition('deleted', 0)
    ->execute();
  foreach ($results as $row) {
    $delta = $row['delta'];
    unset($row['bundle'], $row['delta'], $row['deleted']);
    $keys = array_intersect_key($row, $pkey);
    foreach ($field['columns'] as $name => $spec) {
      $column_name = $field['field_name'] .'_'. $name;
      $row[$column_name .'_'. $delta] = $row[$column_name];
      unset($row[$column_name]);
    }
    db_merge($bundle_table)->fields($row)->key($keys)->execute();
  }
}

/**
 * Implementation of hook_field_attach_pre_load.
 *
 * Load available fields from an object's bundle table, preventing
 * them from being loaded from a field table.
 */
function pbs_field_attach_pre_load($obj_type, $objects, $age, &$skip_fields, $options) {
  $etid = _field_sql_storage_etid($obj_type);
  $load_current = $age == FIELD_LOAD_CURRENT;

  // Gather ids needed for each bundle.
  $bundle_ids = array();
  $delta_count = array();
  foreach ($objects as $obj) {
    list($id, $vid, $bundle) = field_extract_ids($obj_type, $obj);
    $bundle_ids[$bundle][] = $load_current ? $id : $vid;
  }

  // Get the fields to load for each bundle.
  $bundle_fields = variable_get('pbs_field_map', array());

  $new_skips = array();

  foreach ($bundle_ids as $bundle => $ids) {
    // If objects come in with a $bundle that does not exist any more,
    // there will not be any data for them, and $bundle_fields[$bundle] 
    // will not exist.
    if (!isset($bundle_fields[$bundle])) {
      continue;
    }

    // Load the data for all objects in $bundle.
    $table = $load_current ? pbs_tablename($bundle) : pbs_revision_tablename($bundle);
    $results = db_select($table, 't')
      ->fields('t')
      ->condition('etid', $etid)
      ->condition($load_current ? 'entity_id' : 'revision_id', $ids, 'IN')
      ->execute();

    // For each row, for every field in the bundle, construct field
    // data items from the columns for that field, and save the items
    // keyed by object id and field name.
    foreach ($results as $row) {
      foreach ($bundle_fields[$bundle] as $column_name => $tuple) {
        list($field_name, $field_id, $item_name, $delta) = $tuple;

        if (!isset($skip_fields[$field_id]) && (!isset($options['field_id']) || $options['field_id'] == $field_id)) {
          if (!is_null($row->{$column_name})) {
            // We must explicitly include $delta in the assignment
            // below.  Consider a field 'foo' with two columns, e.g. value
            // and format. Without using $delta, the row containing
            // foo_value_0 and foo_format_0 would end up stored as
            // $obj->foo[0]['value'] and $obj->foo[1/*BZZZ!*/]['format'].
            $objects[$row->entity_id]->{$field_name}[$row->language][$delta][$item_name] = $row->{$column_name};
          }
          else if (!isset($objects[$row->entity_id]->{$field_name})) {
            // Even if we have no data for a field, $obj->field should
            // always at least be an empty array.  Do NOT put a
            // language key in; if we have no data for a field, it is wrong to
            // create an empty language key for it.
            $objects[$row->entity_id]->{$field_name} = array();
          }
          $new_skips[$field_id] = 1;
        }
      }
    }
  }

  $skip_fields += $new_skips;
}

/**
 * Implementation of field_attach_pre_update.
 *
 * When an object's fields are saved, update its bundle table and
 * add saved field names as keys to $skip_fields.
 *
 * If the field is not shared, tell the field storage not to store it
 * normally.  If the field is shared, let the field storage system
 * store it.  The idea here is that we want to allow querying the
 * field to require querying only a single table.  If we stored a
 * shared field in multiple bundle tables and prevented per-field
 * storage, querying would have to query multiple per-bundle tables.
 */
function pbs_field_attach_pre_update($obj_type, $object, &$skip_fields) {
  $etid = _field_sql_storage_etid($obj_type);
  list($id, $vid, $bundle, $cacheable) = field_extract_ids($obj_type, $object);
  $bundle_table = pbs_tablename($bundle);
  $revision_table = pbs_revision_tablename($bundle);
  $all_fields = array();
  $bundle_fields = variable_get('pbs_field_map', array());

  $key = array('etid' => $etid, 'entity_id' => $id, 'revision_id' => $vid);

  $instances = field_info_instances($bundle);
  $rows = array();
  foreach ($instances as $instance) {
    // The Rules:
    // * $object->field_name unset: leave data untouched
    // * $object->field_name = array()/NULL: empty the field for all
    //   (enabled) languages 
    // * $object->field_name[$lang] unset: leave data untouched for $lang
    // * $object->field_name[$lang] = array()/NULL: empty the field for $lang
    //
    // Enabled/available languages for a single field are returned by
    // field_multilingual_available_languages.

    // Function property_exists() is slower, so we catch the more frequent cases
    // where it's an empty array with the faster isset().
    $field_name = $instance['field_name'];
    $field = field_info_field($instance['field_name']);
    $field_id = $field['id'];
    if (!isset($skip_fields[$field['id']]) && $field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
      if (isset($object->$field_name) || property_exists($object, $field_name)) {
        // For each field, we are given data for one or more language
        // keys. The rule is to update data for the languages we're
        // given, and leave alone data for languages we're not
        // given. We write all the data for each language on a single
        // row, but we do not want to assume we have the same language
        // keys for every field and cannot assume that we have all the
        // fields for any language for which we have one field.  
        //
        // So, we build up an array $rows keyed by language. Each
        // $rows[$language] contains all the columns for all the
        // fields which have data for that language, and no entries
        // for the other columns.

        // TODO: The field might be NULL or array().  I need to figure out the
        // correct behavior in that case; I think it will be UPDATEing
        // all language rows for the object to have NULL for that
        // field.  For now, just avoid an exception.
        if (empty($object->{$field_name})) {
          // if $object->$field_name is null/empty I think all you
          // need to do is set NULL the field values for every row
          // whose language is in the set returned by
          // field_multilingual_available_languages($obj_type, $field) 
          $langs_items = array_fill_keys(field_multilingual_available_languages($obj_type, $field), array());
        }
        else {
          $langs_items = $object->{$field_name};
        }

        foreach ($langs_items as $language => $items) {
          // $obj->field[lang] can be NULL
          if ($items === NULL) {
            $items = array();
          }

          // field_attach_load() is specified to return data items indexed
          // from delta 0, regardless of how they were provided on
          // save.  It is more efficient to re-index on save than on load.
          $items = array_merge($items);
          for ($delta = 0; $delta < $field['cardinality']; ++$delta) {
            foreach ($field['columns'] as $name => $spec) {
              $rows[$language][_field_sql_storage_columnname($field['field_name'], $name) .'_'. $delta] = isset($items[$delta][$name]) ? $items[$delta][$name] : NULL;
            }
          }
        }
      }

      // Whether we left it untouched, emptied it, or stored it, we
      // still handled it.  Tell the field storage module to skip it,
      // but only if it is a non-shared field.
      if (!PBS_DUPLICATE_SHARED_FIELDS || count($field['bundles']) == 1) {
        $all_fields[$field_id] = 1;
      }
    }
  }

  foreach ($rows as $language => $row) {
    if (count($row)) {
      $row['language'] = $key['language'] = $language;
      db_merge($bundle_table)->fields($row)->key($key)->execute();
      if (isset($vid)) {
        db_merge($revision_table)->fields($row)->key($key)->execute();
      }
    }
  }

  if (PBS_PRIMARY_STORAGE) {
    $skip_fields += $all_fields;
  }
}

/**
 * Implementation of hook_field_attach_pre_insert.
 *
 * When an object's fields are saved, update its bundle table and
 * add saved field names as keys to $skip_fields.
 */
function pbs_field_attach_pre_insert($obj_type, $object, &$skip_fields) {
  return pbs_field_attach_pre_update($obj_type, $object, $skip_fields);
}

/**
 * Implementation of hook_field_attach_delete.
 */
function pbs_field_attach_delete($obj_type, $object) {
  $etid = _field_sql_storage_etid($obj_type);
  list($id, $vid, $bundle, $cacheable) = field_extract_ids($obj_type, $object);
  $bundle_table = pbs_tablename($bundle);
  $revision_table = pbs_revision_tablename($bundle);
  db_delete($bundle_table)->condition('etid', $etid)->condition('entity_id', $id)->execute();
  db_delete($revision_table)->condition('etid', $etid)->condition('entity_id', $id)->execute();
}

/**
 * Implementation of field_attach_delete_revision.
 */
function pbs_field_attach_delete_revision($obj_type, $object) {
  $etid = _field_sql_storage_etid($obj_type);
  list($id, $vid, $bundle, $cacheable) = field_extract_ids($obj_type, $object);
  $revision_table = pbs_revision_tablename($bundle);
  db_delete($revision_table)->condition('etid', $etid)->condition('entity_id', $id)->condition('revision_id', $vid)->execute();
}

/**
 * Implementation of hook_field_create_instance.
 *
 * When a field is added to a bundle, add the new columns to the
 * bundle table.
 */
function pbs_field_create_instance($instance) {
  pbs_precompute_bundle_map();

  // If the bundle table does not exist yet, create it now.  The
  // initial schema for the bundle table will include the columns for
  // the instance just created; we do not need to do it again here.
  if (!db_table_exists(pbs_tablename($instance['bundle']))) {
    pbs_create_bundle_tables($instance['bundle']);
    return;
  }

  $field = field_info_field($instance['field_name']);
  if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
    $bundle_table = pbs_tablename($instance['bundle']);
    $revision_table = pbs_revision_tablename($instance['bundle']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    for ($i = 0; $i < $field['cardinality']; ++$i) {
      foreach ($schema['columns'] as $name => $spec) {
        db_add_field($ret, $bundle_table, $field['field_name'] .'_'. $name .'_'. $i, $spec);
        db_add_field($ret, $revision_table, $field['field_name'] .'_'. $name .'_'. $i, $spec);
      }
    }
  }
}

/**
 * Implementation of hook_field_delete_instance.
 *
 * When a field is removed from a bundle, drop the columns and update
 * the bundle map. We have to remove the columns immediately because a
 * new field with the same name may be created later.
 *
 * To support being primary storage, we would have rename the columns
 * instead and clean them up on batch delete.
 */
function pbs_field_delete_instance($instance) {
  $ret = array();

  $field = field_info_field($instance['field_name']);
  if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED) {
    $bundle_table = pbs_tablename($instance['bundle']);
    $revision_table = pbs_revision_tablename($instance['bundle']);
    $schema = (array) module_invoke($field['module'], 'field_schema', $field);
    for ($i = 0; $i < $field['cardinality']; ++$i) {
      foreach ($schema['columns'] as $name => $spec) {
        db_drop_field($ret, $bundle_table, $instance['field_name'] .'_'. $name .'_'. $i);
        db_drop_field($ret, $revision_table, $instance['field_name'] .'_'. $name .'_'. $i);
      }
    }
  }

  pbs_precompute_bundle_map();
}

/**
 * Implementation of hook_field_attach_create_bundle.
 *
 * Create the (empty) bundle tables.
 */
function pbs_field_attach_create_bundle($bundle) {
  pbs_precompute_bundle_map();
  pbs_create_bundle_tables($bundle);
}

/**
 * Implementation of hook_field_attach_rename_bundle.
 *
 * Rename the bundle tables.
 */
function pbs_field_attach_rename_bundle($old_bundle, $new_bundle) {
  $ret = array();
  db_rename_table($ret, pbs_tablename($old_bundle), pbs_tablename($new_bundle));
  db_rename_table($ret, pbs_revision_tablename($old_bundle), pbs_revision_tablename($new_bundle));
  pbs_precompute_bundle_map();
}

/**
 * Implementation of hook_field_attach_delete_bundle.
 *
 * When a bundle is deleted bundle, drop the bundle tables and update
 * the bundle map.  We have to drop the tables immediately because a
 * new bundle with the same name may be created later.
 *
 * To support being primary storage, we would have rename the tables
 * instead and clean them up on batch delete.
 */
function pbs_field_attach_delete_bundle($bundle) {
  $ret = array();
  db_drop_table($ret, pbs_tablename($bundle));
  db_drop_table($ret, pbs_revision_tablename($bundle));
  pbs_precompute_bundle_map();
}

